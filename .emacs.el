(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t)
 '(package-selected-packages '(slime-repl-ansi-color slime-docker slime ## evil)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

; use-package
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; I can do the exact same thing to install a theme automatically.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


; YASnippet
(use-package yasnippet
    :ensure t
    :config
    (use-package yasnippet-snippets
    :ensure t)
    (yas-reload-all)
    (yas-global-mode +1))

;;; PACKAGE LIST
(setq package-archives
     '(("melpa" . "https://melpa.org/packages/")
       ("elpa" . "https://elpa.gnu.org/packages/")
       ("nongnu" . "https://elpa.nongnu.org/nongnu/")))

; evil mode
(require 'evil)
(evil-mode 1)

; Displays numbers
(global-display-line-numbers-mode)

; Automatic (); []; {} parining
(electric-pair-mode)

; Parentheses highlight
(show-paren-mode)

; Theme
(load-theme 'wheatgrass)
